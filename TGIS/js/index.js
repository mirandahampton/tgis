﻿$(document).ready(function () {
    $('.js-example-basic-multiple').select2({
        tags: true
    });

    $.get('/api/TGIS/GetTagDropDown')
        .done(function (result) {
            if (result)
                document.getElementById("eventTags").innerHTML = result;
        })
        .fail(function () {
            alert("fail");
        });

    $('#yearRange').change(function () {
        addLines();
    });

    //sourceSlider();

});

$(document).on('input', '#yearRange', function () {
    var newYear = $(this).val();
    loadMap(newYear);

});

var map;
function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(40, 21),
        zoom: 5, //0-21
        minZoom: 3,
        //OGI Custom Map Style
        styles: [{ "featureType": "administrative.country", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }, { "weight": "0.60" }] }, { "featureType": "administrative.country", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.province", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.locality", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.neighborhood", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.land_parcel", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "administrative.land_parcel", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "geometry.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.natural.landcover", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "landscape.natural.terrain", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.arterial", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.local", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "labels", "stylers": [{ "visibility": "off" }] }],
        mapTypeId: google.maps.MapTypeId.HYBRID,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        scaleControl: true,  // fixed to BOTTOM_RIGHT
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        }
    };

    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

    //Initialize VisLayer
    VisLayer = new google.maps.Data();
    LineVisLayer = new google.maps.Data();
    loadMap(1000);
    addLines();

    google.maps.event.addListener(map, 'click', function (event) {
        $('#eventLat').val(event.latLng.lat());
        $('#eventLon').val(event.latLng.lng());

    });

}

google.maps.event.addDomListener(window, 'load', initialize);

var prev_InfoWindow = new google.maps.InfoWindow({
    content: "",
    maxWidth: 400
});

function sourceSlider() {
    $.ajax({
        type: 'GET',
        url: '/api/TGIS/SourceSlider',
        cache: false,
        success: function (result) {
            document.getElementById("headerDataPanel").innerHTML = result;
            mcThumbnailSlider.display(0);
        }
    });
}

function SubmitEvent() {
    var title = $('#eventTitle').val();
    var narrative = $('#eventNarrative').val();
    var lat = $('#eventLat').val();
    lat = parseFloat(lat);
    var lon = $('#eventLon').val();
    lon = parseFloat(lon);
    var year = $('#eventYear').val();
    year = parseInt(year);
    
    var tags = $('#eventTags').val();
    var tagstr = "";
    for (var i = 0; i < tags.length; i++) {
        tagstr += tags[i] + "^";
    }

    $.ajax({
        type: 'POST',
        url: '/api/TGIS/AddEvent?&title=' + title + '&narrative=' + narrative + '&lat=' + lat + '&lon=' + lon + '&year=' + year + '&tagstr=' + tagstr,
        cache: false,
        success: function () {
            loadMap(1000);
            $('#eventTitle').val("");
            $('#eventNarrative').val("");
            $('#eventLat').val("");
            $('#eventLon').val("");
            $('#eventYear').val("");
            $('#eventTags').val(null).trigger('change');
        }
    });
}


function loadMap(year) {
    google.maps.event.addDomListener(map, 'click', function () {
        prev_InfoWindow.close();
    });
    $.get('/api/TGIS/PopulateMap?compareYear=' + year)
        .done(function (data) {
            var GeoJSONString = JSON.parse(data.DynamicMap);
            VisLayer.setMap(null);
            VisLayer = new google.maps.Data();
            VisLayer.addGeoJson(GeoJSONString);

            //InfoWindow
            infoWindow = new google.maps.InfoWindow({
                content: "",
                maxWidth: 400
            });
            if (prev_InfoWindow !== null)
                prev_InfoWindow.close();
            VisLayer.addListener('click', function (event) {
                var id = event.feature.getProperty('ID');
                if (event.feature.getProperty('GeometryType') === 'dot') {
                    //When you build this out for real, line and circles should have different postmodels
                    if (prev_InfoWindow !== null)
                        prev_InfoWindow.close();
                    circleInfo(event);
                }
                //else {
                //    if (prev_InfoWindow != null)
                //        prev_InfoWindow.close();
                //    lineInfo(event);
                //}
            });



            // Apply styles
            VisLayer.setStyle(function (feature) {
                var GeometryType = feature.getProperty('GeometryType');
                var color = '#ff0000';
                if (GeometryType === "dot") {
                    var circScale = 24;
                var Mag = feature.getProperty('Mag');
                    if (Mag > 500) { circScale = 4; }
                    if (Mag <= 500 && Mag > 400) { circScale = 8; }
                    if (Mag <= 400 && Mag > 300) { circScale = 12; }
                    if (Mag <= 300 && Mag > 200) { circScale = 16; }
                    if (Mag <= 200 && Mag > 100) { circScale = 20; }
                    if (Mag <= 100 && Mag >= 0) { circScale = 24; }
                    return ({
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: circScale,
                            fillColor: color,
                            fillOpacity: .5,
                            strokeWeight: 1,
                            strokeColor: color
                        }
                    });
                }

                if (GeometryType === "line") {
                    var thisStroke = 1;
                    var opacity = 1;
                    //thisStroke = 5;
                    //if (Mag > 1.0) { thisStroke = 10; opacity = 0; };
                    //if (Mag <= 1.0 && Mag > .95) { thisStroke = 5; };
                    //if (Mag <= .95 && Mag > .75) { thisStroke = 4; };
                    //if (Mag <= .75 && Mag > .5) { thisStroke = 3; };
                    //if (Mag <= .5 && Mag > .25) { thisStroke = 2; };
                    //if (Mag <= .25 && Mag > 0.0) { thisStroke = 1; };
                    return ({
                        geodesic: false,
                        strokeOpacity: opacity,
                        strokeWeight: thisStroke,
                        strokeColor: '#FFFFFF'
                    });
                }
            });

            VisLayer.setMap(map);

        })

        .fail(function (jqXHR, textStatus, err) {
            $('map-canvas').innerHTML = 'Error: ' + err;
        });
}

function addLines() {
    $.ajax({
        type: 'GET',
        url: '/api/TGIS/AddLines',
        cache: false,
        success: function (data) {
            var GeoJSONString = JSON.parse(data.DynamicMap);
            LineVisLayer.setMap(null);
            LineVisLayer = new google.maps.Data();
            LineVisLayer.addGeoJson(GeoJSONString);

            // Apply styles
            LineVisLayer.setStyle(function (feature) {
                var GeometryType = feature.getProperty('GeometryType');
                if (GeometryType === "line") {
                    var thisStroke = 1;
                    var opacity = 1;
                    return {
                        geodesic: false,
                        strokeOpacity: opacity,
                        strokeWeight: thisStroke,
                        strokeColor: '#FFFFFF'
                    };
                }
            });

            LineVisLayer.setMap(map);

        }
    });
}

function circleInfo(event) {
    var id = event.feature.getProperty('ID');
    var anchor = new google.maps.MVCObject();
    anchor.set("position", event.latLng);
    var infoWindow = new google.maps.InfoWindow({
        content: "",
        maxWidth: 400
    });
    $.ajax({
        type: 'POST',
        url: '/api/TGIS/CircleInfoWindow?&Id=' + id,
        cache: false,
        success: function (result) {
            infoWindow.setContent(result);
            prev_InfoWindow = infoWindow;
            prev_InfoWindow.open(map, anchor);
        }
    });
}