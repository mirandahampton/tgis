﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Security.Cryptography;

namespace TGIS.Controllers
{
    public class TGISController : ApiController
    {

        //Draw Events on Map
        [HttpGet]
        public IHttpActionResult PopulateMap(int compareYear)
        {
            //Start the GeoJSON Object
            StringBuilder sb = new StringBuilder();
            sb.Append("{ \"type\": \"FeatureCollection\", \"features\": [ ");

            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("PopulateMap", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string ID = r[0].ToString();
                    string eventTitle = r[1].ToString();
                    string Narrative = r[2].ToString();
                    string Lat = r[3].ToString();
                    string Lon = r[4].ToString();
                    string Year = r[5].ToString();
                    int Mag = (Math.Abs(compareYear - Convert.ToInt32(Year)) + 50) / 100 * 100;
                    sb.Append(" { \"type\": \"Feature\", \"geometry\": {\"type\": \"Point\", \"coordinates\": [" + Lon + ", " + Lat + "]}, \"properties\": {\"ID\": \"" + ID + "\", \"GeometryType\": \"dot\", \"Mag\": \"" + Mag + "\"} },");
                }
            }

            //Close up
            sb.Append("]}");
            string FinalString = sb.ToString();
            FinalString = FinalString.Replace(",]}", "]}"); //remove that pesky comma

            //Return the Dynamic Map
            Map_VisLayer[] thisMap = new Map_VisLayer[1];
            thisMap[0] = new Map_VisLayer();
            thisMap[0].DynamicMap = FinalString;
            var ResultSet = thisMap.FirstOrDefault();
            return Ok(ResultSet);

        }

        //Draw Events on Map
        [HttpGet]
        public IHttpActionResult AddLines()
        {
            //Start the GeoJSON Object
            StringBuilder sb = new StringBuilder();
            sb.Append("{ \"type\": \"FeatureCollection\", \"features\": [ ");

            //Draw the NetworkMap
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("TagConnections", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string ID = r[0].ToString();
                    string Tag = r[1].ToString();
                    string Event1 = r[2].ToString();
                    string Event2 = r[3].ToString();
                    string Lat_A = r[4].ToString();
                    string Lon_A = r[5].ToString();
                    string Lat_B = r[6].ToString();
                    string Lon_B = r[7].ToString();


                    sb.Append("{ \"type\": \"Feature\", \"geometry\": {\"type\": \"LineString\", \"coordinates\": [[" + Lon_A + ", " + Lat_A + "], [" + Lon_B + ", " + Lat_B + "]]}, \"properties\": {\"ID\":" + ID + ", \"ZIndex\": 1, \"GeometryType\": \"line\", \"ToggleOn\": \"true\"} },");
                }
            }

            //Close up
            sb.Append("]}");
            string FinalString = sb.ToString();
            FinalString = FinalString.Replace(",]}", "]}"); //remove that pesky comma

            //Return the Dynamic Map
            Map_VisLayer[] thisMap = new Map_VisLayer[1];
            thisMap[0] = new Map_VisLayer();
            thisMap[0].DynamicMap = FinalString;
            var ResultSet = thisMap.FirstOrDefault();
            return Ok(ResultSet);

        }

        [HttpPost]
        public IHttpActionResult CircleInfoWindow(string Id)
        {
            string eventTitle = "", narrative = "", lat = "", lon = "", year = "", tags="";
            StringBuilder sb = new StringBuilder();
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                //Read the form
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_CircleInfoWindow", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("id", Convert.ToInt32(Id));
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    eventTitle = r[1].ToString();
                    narrative = r[2].ToString();
                    lat = r[3].ToString();
                    lon = r[4].ToString();
                    year = r[5].ToString();


                }
            }
            sb.Append("<h3 style=\"text-align: center;\">" + HttpUtility.HtmlEncode(eventTitle) + "</h3>");
            sb.Append("<p><strong>" + HttpUtility.HtmlEncode(narrative) + "</strong></p>");
            sb.Append("<p>" + HttpUtility.HtmlEncode(year) + "</p>");
            sb.Append("</br>");
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                //Read the form
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_TagsForCircleInfo", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("id", Convert.ToInt32(Id));
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    tags += r[0].ToString() + ", ";

                }
            }
            sb.Append("<h6>Tags: ");
            int index = tags.LastIndexOf(',');
            if (index > 0)
                tags = tags.Remove(index, 1);
            sb.Append(HttpUtility.HtmlEncode(tags) + "</br></h6>");
            sb.Append("</br>");


            string returnString = sb.ToString();
            return Ok(returnString);
        }

        [HttpGet]
        public IHttpActionResult SourceSlider()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ul>");
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                //Read the form
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_PopulateSourceSlider", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    sb.Append("<li>");
                    sb.Append("<a class=\"thumb srcinactive\" href=\"/img/" + r[6].ToString() + ".png\" id=\""+ r[0].ToString() + "src\" onclick=\"ToggleMap('" + r[0].ToString() + "src\'); \"></a>");
                    sb.Append("</li>");
                }
            }

            sb.Append("</ul>");

            string returnString = sb.ToString();
            return Ok(returnString);
        }

        [HttpGet]
        public String GetTagDropDown()
        {
            StringBuilder sb = new StringBuilder();

            //Call sp_BlackDogWeightedMap
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                //Read the form
                Conn.Open();
                SqlCommand Com = new SqlCommand("SELECT * FROM TGIS_Tags", Conn);
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    sb.Append("<option id=\"" + r[0].ToString() + " \" value=\"" + r[1].ToString() + "\">" + r[1].ToString() + "</option>");
                }
            }

            return sb.ToString();
        }


        [HttpPost]
        public void AddEvent(string title, string narrative, double Lat, double Lon, int year, string tagstr)
        {
            string[] tags = tagstr.Split('^');
            string id = "";
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                //Read the form
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_AddEvent", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("@EventTitle", title);
                Com.Parameters.AddWithValue("@Narrative", narrative);
                Com.Parameters.AddWithValue("@Lat", Lat);
                Com.Parameters.AddWithValue("@Lon", Lon);
                Com.Parameters.AddWithValue("@Year", year);
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    id = r[0].ToString();
                }
            }
            for (int i = 0; i < tags.Length - 1; i++)
            {
                using (SqlConnection Conn2 = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
                {
                    //Read the form
                    Conn2.Open();
                    SqlCommand Com2 = new SqlCommand("AddTag", Conn2);
                    Com2.CommandType = CommandType.StoredProcedure;
                    Com2.Parameters.AddWithValue("@Tag", tags[i]);
                    Com2.Parameters.AddWithValue("@id", Convert.ToInt32(id));
                    Com2.ExecuteNonQuery();
                }
            }
            return;
        }

        public static string photoName(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(maxSize);
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("SELECT COUNT(*) FROM Borderlands_Uploads", Conn);
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    result.Append(r[0].ToString());
                }
            }
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
    }

    //----------------------------------------------------
    // DATA MODELS
    //----------------------------------------------------
    public class Map_VisLayer
    {
        public string DynamicMap { get; set; }
    }
}
