﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

namespace TGIS
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ul>");
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                //Read the form
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_PopulateSourceSlider", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    sb.Append("<li>");
                    sb.Append("<a class=\"thumb srcinactive\" href=\"/img/" + r[6].ToString() + ".png\" id=\"" + r[0].ToString() + "src\" onclick=\"ToggleMap('" + r[0].ToString() + "src'); \"><i class=\"fa fa - user\"></i> </a>");
                    sb.Append("</li>");
                }
            }

            sb.Append("</ul>");

            string returnString = sb.ToString();
            headerDataPanel.InnerHtml = returnString;
        }
    }
}