﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="TGIS.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <%-- Javascript --%>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/index.css" rel="stylesheet" />
    <link href="css/select2.min.css" rel="stylesheet" />
    <link href="css/thumbnail-slider.css" rel="stylesheet" />
    <link href="css/thumbs2.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <style>
  html, body, #map-canvas {
    height: 100%;
  }
</style>

</head>

<body>

    <%-- Implement an hour glass model for use with the slower drawing UA Global Alumni map (loads the gif while data is being fetched and dims  the map (Ganesh Raikhelkar October 2016) --%>
<%--    <div id="loading">
        <img id="loading_box" src="/img/Modalhour58.gif" alt="Hour glass"/></div>--%>


    <%-- DataPanel.  This is the main navigational element.  It looks like a classic "Map Legend" which floats (and can be moved) upon the map but it actually is a dynamic and extensible component which is the home to all controls related to exploring individual data sets and filtering upon their unique properties.  This relatively complex component is what makes Maps.Global appear to be such a 'simple' application.  All functionality is contained within this component, and the results of its use are drawn to the map.--%>

    <div id="DataPanel">
        <div id="DataPanelInfo">
            <%-- DataPanel Heading --%>
            <div class="DataPanel_Header"><b>TGIS</b></div>
            <div id="thumbnail-slider">
            <div class="inner" runat="server" id="headerDataPanel">

            </div>
        </div>


            <%-- Can do from code behind but no data should be coming through so I'm hard coding it--%>
            <div id="DataEntries">
                <div id="eventbar">
                    <label for="eventTitle">Event:</label><input id="eventTitle" type="text" />
                </div>
                <div id="narrativebar">
                    <label for="eventNarrative">Narrative:</label><input id="eventNarrative" type="text" />
                </div>
                <div id="LatLon">
                    <label for="eventLat">Lat:</label><input id="eventLat" type="number" min="-90" max="90" step="0.0001"/>
                    <label for="eventLon">Lon:</label><input id="eventLon" type="number" min="-180" max="180" step="0.0001"/>
                </div>
                <div id="year">
                    <label for="eventYear">Year:</label><input id="eventYear" type="number" min="1" max="2020" />
                </div>
                <div id="phototags">
                    <span>Tags:</span>
                    <select class="js-example-basic-multiple" name="tags" id="eventTags" style="width: 75%" multiple="multiple" data-validate="required" data-name="tag" data-error="Tags is a required field" required="required">

                    </select>
                </div>
                <div id="submitbtn">
                    <button class="right" onclick="SubmitEvent()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div id="slider" class="range-field">
        <input type="range" min="0" max="2020" value="1000" step="100" class="slider" id="yearRange" />
    </div>


    <%-- The Google Map --%>
    <div id="map-canvas"></div>

        <script async="async" defer="defer" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbQmUjsLKqbolxPJsKcei5ihsFb6E50ew"></script>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/index.js"></script>
    <script src="js/thumbnail-slider.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">

	    <%-- Call Document Ready --%>
    $(document).ready(function () {
        $("#DataPanel").draggable(); //Make the DataPanel draggable
        VisLayer = new google.maps.Data(); //Initialize the VisLayer object
    });

    //Goolge Analytics
    (function (i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-31635729-6', 'auto');
    ga('send', 'pageview');
    </script>
</body>
</html>
